import type { Config } from "tailwindcss";

const config: Config = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./components/**/*.{js,ts,jsx,tsx,mdx}",
    "./app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  darkMode: "media",
  theme: {
    extend: {
      colors: {
        primary: "#60316e",
        secondary: "#93566f",
        dark: "#0f090b",
        darker: "#1e1117",
        light: "#ead6de",
        lighter: "#f4eaee",
      },
    },
  },

  plugins: [],
};

export default config;
