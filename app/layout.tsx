import type { Metadata, Viewport } from "next";
import Script from "next/script";

import "tailwindcss/tailwind.css";

export const viewport: Viewport = {
  width: "device-width",
  initialScale: 1,
};

export const metadata: Metadata = {
  title: "Daniel Brüggemann | Web developer",
  description:
    "A Freelance Web Developer from Nennhausen (Berlin Region) with emphasis on the web. Works best with Typescript, Tests and Neovim.",
  icons: {
    icon: {
      url: "favicon.svg",
      type: "image/svg",
    },
    shortcut: { url: "/favicon.svg", type: "image/svg" },
  },
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en" className="h-full" suppressHydrationWarning={true}>
      <Script
        defer
        type="text/javascript"
        src="https://api.pirsch.io/pirsch.js"
        id="pirschjs"
        data-code="9GtXIINLh4uG4L7umddzu58zAirFK20z"
      />
      <body className="bg-neutral-100 dark:bg-neutral-900 text-dark dark:text-light font-sans antialiased">
        {children}
      </body>
    </html>
  );
}
