import { type ReactNode } from "react";

interface Props {
  children: ReactNode;
  rel?: string;
  href: string;
  title: string;
}

const SIZE = 24;

export function LinkedIcon({ children, rel, href, title }: Props) {
  return (
    <a
      rel={rel}
      href={href}
      className="text-dark dark:text-light rounded-xl p-2 hover:text-primary"
      aria-label={title}
    >
      <svg
        xmlns="http://www.w3.org/2000/svg"
        role="img"
        width={SIZE}
        height={SIZE}
        viewBox="0 0 24 24"
        fill="currentColor"
        aria-labelledby={title}
      >
        <title id={title}>{title}</title>
        {children}
      </svg>
    </a>
  );
}
