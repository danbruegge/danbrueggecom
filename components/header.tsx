import Image from "next/image";
import meImage from "../public/me.webp";

export function Header() {
  return (
    <header className="p-[2px] rounded-xl bg-gradient-to-br from-primary to-secondary">
      <div className="flex gap-8 sm:items-center flex-col sm:flex-row p-8 sm:p-4 rounded-xl bg-lighter dark:bg-dark">
        <p className="flex justify-center sm:justify-left">
          <Image
            className="rounded-lg"
            src={meImage.src}
            alt="Image of Daniel Brüggemann with a beard and a red shirt."
            width="320"
            height="320"
          />
        </p>

        <div>
          <p className="text-2xl text-justify">
            Greetings! I&apos;m Daniel 👋 - a <i>Freelance Web Developer</i>{" "}
            specialized in frontend development, with a primary focus on{" "}
            <i>React.js</i> and everything around it.
          </p>
        </div>
      </div>
    </header>
  );
}
